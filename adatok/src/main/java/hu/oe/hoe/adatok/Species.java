package hu.oe.hoe.adatok;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Species")
public class Species {
 
    //id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id;    
    
    //fields
    private String name;
    private String description;
    
    //constructors
    public Species(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Species() {
    }
    
    //get/set-s
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
