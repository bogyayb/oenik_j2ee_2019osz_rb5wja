package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Hero")
public class Hero {
 
    //id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id; 
    
    //fields
    private String name;
    private String description;
    
    //relations
    @ManyToOne
    private User user;
    
    @OneToMany
    private List<Hybrid> hybrids = new ArrayList<>();
    
    //constructors
    public Hero(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Hero() {
        
    }
        
    //get/set-s
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Hybrid> getHybrids() {
        return hybrids;
    }

    public void setHybrids(List<Hybrid> hybrids) {
        this.hybrids = hybrids;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
}
