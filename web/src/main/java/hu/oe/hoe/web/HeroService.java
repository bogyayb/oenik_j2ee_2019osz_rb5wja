package hu.oe.hoe.web;

import hu.oe.hoe.adatok.Hero;
import hu.oe.hoe.adatok.HeroRepository;
import hu.oe.hoe.adatok.Hybrid;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless //állapotmentes, mindig újrapéldányosodik, ha szükséges
public class HeroService {
    
    //injects
    @Inject
    HeroRepository heroRepository;
    
    //methods
    public Hero add(Hero hero) {
        byte sum = 0;
        
        if(hero != null && hero.getHybrids() != null) {
            for(Hybrid h: hero.getHybrids()) {
                sum += h.getPercent();
            }
        }
        
        //hibdid % ellenőrzés
        if(sum != 100) {
            throw new ValidateHeroRuntimeException();
        }
        
        //duplikáció megelőzés
        if(heroRepository.getByNameUser(hero.getName(), hero.getUser()).size() > 0) {
            throw new ValidateHeroRuntimeException();
        }
        
        heroRepository.Add(hero);
        return hero;
    }
    
    public List<Hero> getHeroes() {
        return heroRepository.getHeroes();
    }
}
